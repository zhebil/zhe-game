const constants = {
  ROUTES: {
    TRUTH: '/',
    NEVER: '/never',
    QUESTIONS: '/questions',
    SELECT_PLAYER: '/select-players',
    ADMIN_PAGE: '/admin',
    PRESETS: '/presets',
  },
} as const;

export default constants;
